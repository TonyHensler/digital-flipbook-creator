<?php
session_start();
if($_SESSION['loggedin'] != 'DamStraight' || !isset($_SESSION['loggedin'])) {
	header('Location: login.php');
 }
ini_set('upload_tmp_dir','uploads/'); 
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Tony Hensler">

    <title>Digital Flipbook Publication Builder - Create Publications</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">


    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php">Digital Publications</a>
            </div>
            <!-- /.navbar-header -->


            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.php"><i class="fa fa-table fa-fw"></i> Publications</a>
                        </li>
                        <li>
                            <a href="build.php"><i class="fa fa-edit fa-fw"></i> Create</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create Publication</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">

						 <?php
						if(isset($_FILES['userfile'])){
							$uploaddir = 'uploads/';
							$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

							$file = basename($_FILES['userfile']['name']);

							$imageFileType = pathinfo($uploadfile,PATHINFO_EXTENSION);

							$folder_name = substr($file, 0, strrpos( $file, '.') );

							$pubName = ucfirst(str_replace("_", " ", $folder_name));

							print_r($_FILES['userfile']);

							$folder =  "publications/".substr($file, 0, strrpos( $file, '.') )."/";
							echo "<p>";

							shell_exec("cp -a template/. publications/$folder_name");

							echo shell_exec("whoami")."-1";

							if (move_uploaded_file($_FILES['userfile']['tmp_name'], $folder.$file)) {
							  	echo "File is valid, and was successfully uploaded.\n";

							  	shell_exec("gs -dNOPAUSE -sDEVICE=jpeg -r288 -sOutputFile=".$folder."/pages/%01d-large.jpg ".$folder."/".$file." && gs -dNOPAUSE -sDEVICE=jpeg -r288 -sOutputFile=".$folder."/pages/%01d-thumb.jpg ".$folder."/".$file." && gs -dNOPAUSE -sDEVICE=jpeg -r288 -sOutputFile=".$folder."/pages/%01d.jpg ".$folder."/".$file);

								$myfile = fopen("publications/$folder_name/index.html", "w") or die("Unable to open file!");
							echo shell_exec("whoami")."-3";


							    $i = 0; 
							    $dir = "publications/$folder_name/pages/";
							    if ($handle = opendir($dir)) {
							        while (($file = readdir($handle)) !== false){
							            if (!in_array($file, array('.', '..')) && !is_dir($dir.$file)) 
							                $i++;
							        }
							    }

							    $pageCount = ($i/3);
							    

								$txt = "
										<!doctype html>
										<!--[if lt IE 7 ]> <html lang='en' class='ie6'> <![endif]-->
										<!--[if IE 7 ]>    <html lang='en' class='ie7'> <![endif]-->
										<!--[if IE 8 ]>    <html lang='en' class='ie8'> <![endif]-->
										<!--[if IE 9 ]>    <html lang='en' class='ie9'> <![endif]-->
										<!--[if !IE]><!--> <html lang='en'> <!--<![endif]-->
										<head>
										<title>$pubName</title>
										<meta name='viewport' content='width = 1050, user-scalable = no' />
										<script type='text/javascript' src='extras/jquery.min.1.7.js'></script>
										<script type='text/javascript' src='extras/modernizr.2.5.3.min.js'></script>
										<script type='text/javascript' src='lib/hash.js'></script>

										</head>
										<body>

										<div id='canvas'>

										<div class='zoom-icon zoom-icon-in'></div>

										<div class='magazine-viewport'>
											<div class='container'>
												<div class='magazine'>
													<!-- Next button -->
													<div ignore='1' class='next-button'></div>
													<!-- Previous button -->
													<div ignore='1' class='previous-button'></div>
												</div>
											</div>
										</div>
										</div>

										<script type='text/javascript'>

										function loadApp() {

										 	$('#canvas').fadeIn(1000);

										 	var flipbook = $('.magazine');

										 	// Check if the CSS was already loaded
											
											if (flipbook.width()==0 || flipbook.height()==0) {
												setTimeout(loadApp, 10);
												return;
											}
											
											// Create the flipbook

											flipbook.turn({
													
													// Magazine width

													width: 922,

													// Magazine height

													height: 600,

													// Duration in millisecond

													duration: 1000,

													// Hardware acceleration

													acceleration: !isChrome(),

													// Enables gradients

													gradients: true,
													
													// Auto center this flipbook

													autoCenter: true,

													// Elevation from the edge of the flipbook when turning a page

													elevation: 50,

													// The number of pages

													pages: $pageCount,

													// Events

													when: {
														turning: function(event, page, view) {
															
															var book = $(this),
															currentPage = book.turn('page'),
															pages = book.turn('pages');
													
															// Update the current URI

															Hash.go('page/' + page).update();

															// Show and hide navigation buttons

															disableControls(page);
															

															$('.thumbnails .page-'+currentPage).
																parent().
																removeClass('current');

															$('.thumbnails .page-'+page).
																parent().
																addClass('current');



														},

														turned: function(event, page, view) {

															disableControls(page);

															$(this).turn('center');

															if (page==1) { 
																$(this).turn('peel', 'br');
															}

														},

														missing: function (event, pages) {

															// Add pages that aren't in the magazine

															for (var i = 0; i < pages.length; i++)
																addPage(pages[i], $(this));

														}
													}

											});

											// Zoom.js

											$('.magazine-viewport').zoom({
												flipbook: $('.magazine'),

												max: function() { 
													
													return largeMagazineWidth()/$('.magazine').width();

												}, 

												when: {

													swipeLeft: function() {

														$(this).zoom('flipbook').turn('next');

													},

													swipeRight: function() {
														
														$(this).zoom('flipbook').turn('previous');

													},

													resize: function(event, scale, page, pageElement) {

														if (scale==1)
															loadSmallPage(page, pageElement);
														else
															loadLargePage(page, pageElement);

													},

													zoomIn: function () {

														$('.thumbnails').hide();
														$('.made').hide();
														$('.magazine').removeClass('animated').addClass('zoom-in');
														$('.zoom-icon').removeClass('zoom-icon-in').addClass('zoom-icon-out');
														
														if (!window.escTip && !$.isTouch) {
															escTip = true;

															$('<div />', {'class': 'exit-message'}).
																html('<div>Press ESC to exit</div>').
																	appendTo($('body')).
																	delay(2000).
																	animate({opacity:0}, 500, function() {
																		$(this).remove();
																	});
														}
													},

													zoomOut: function () {

														$('.exit-message').hide();
														$('.thumbnails').fadeIn();
														$('.made').fadeIn();
														$('.zoom-icon').removeClass('zoom-icon-out').addClass('zoom-icon-in');

														setTimeout(function(){
															$('.magazine').addClass('animated').removeClass('zoom-in');
															resizeViewport();
														}, 0);

													}
												}
											});

											// Zoom event

											if ($.isTouch)
												$('.magazine-viewport').bind('zoom.doubleTap', zoomTo);
											else
												$('.magazine-viewport').bind('zoom.tap', zoomTo);


											// Using arrow keys to turn the page

											$(document).keydown(function(e){

												var previous = 37, next = 39, esc = 27;

												switch (e.keyCode) {
													case previous:

														// left arrow
														$('.magazine').turn('previous');
														e.preventDefault();

													break;
													case next:

														//right arrow
														$('.magazine').turn('next');
														e.preventDefault();

													break;
													case esc:
														
														$('.magazine-viewport').zoom('zoomOut');	
														e.preventDefault();

													break;
												}
											});

											// URIs - Format #/page/1 

											Hash.on('^page\/([0-9]*)$', {
												yep: function(path, parts) {
													var page = parts[1];

													if (page!==undefined) {
														if ($('.magazine').turn('is'))
															$('.magazine').turn('page', page);
													}

												},
												nop: function(path) {

													if ($('.magazine').turn('is'))
														$('.magazine').turn('page', 1);
												}
											});


											$(window).resize(function() {
												resizeViewport();
											}).bind('orientationchange', function() {
												resizeViewport();
											});

											// Events for thumbnails

											$('.thumbnails').click(function(event) {
												
												var page;

												if (event.target && (page=/page-([0-9]+)/.exec($(event.target).attr('class'))) ) {
												
													$('.magazine').turn('page', page[1]);
												}
											});

											$('.thumbnails li').
												bind($.mouseEvents.over, function() {
													
													$(this).addClass('thumb-hover');

												}).bind($.mouseEvents.out, function() {
													
													$(this).removeClass('thumb-hover');

												});

											if ($.isTouch) {
											
												$('.thumbnails').
													addClass('thumbanils-touch').
													bind($.mouseEvents.move, function(event) {
														event.preventDefault();
													});

											} else {

												$('.thumbnails ul').mouseover(function() {

													$('.thumbnails').addClass('thumbnails-hover');

												}).mousedown(function() {

													return false;

												}).mouseout(function() {

													$('.thumbnails').removeClass('thumbnails-hover');

												});

											}


											// Regions

											if ($.isTouch) {
												$('.magazine').bind('touchstart', regionClick);
											} else {
												$('.magazine').click(regionClick);
											}

											// Events for the next button

											$('.next-button').bind($.mouseEvents.over, function() {
												
												$(this).addClass('next-button-hover');

											}).bind($.mouseEvents.out, function() {
												
												$(this).removeClass('next-button-hover');

											}).bind($.mouseEvents.down, function() {
												
												$(this).addClass('next-button-down');

											}).bind($.mouseEvents.up, function() {
												
												$(this).removeClass('next-button-down');

											}).click(function() {
												
												$('.magazine').turn('next');

											});

											// Events for the next button
											
											$('.previous-button').bind($.mouseEvents.over, function() {
												
												$(this).addClass('previous-button-hover');

											}).bind($.mouseEvents.out, function() {
												
												$(this).removeClass('previous-button-hover');

											}).bind($.mouseEvents.down, function() {
												
												$(this).addClass('previous-button-down');

											}).bind($.mouseEvents.up, function() {
												
												$(this).removeClass('previous-button-down');

											}).click(function() {
												
												$('.magazine').turn('previous');

											});


											resizeViewport();

											$('.magazine').addClass('animated');

										}

										// Zoom icon

										 $('.zoom-icon').bind('mouseover', function() { 
										 	
										 	if ($(this).hasClass('zoom-icon-in'))
										 		$(this).addClass('zoom-icon-in-hover');

										 	if ($(this).hasClass('zoom-icon-out'))
										 		$(this).addClass('zoom-icon-out-hover');
										 
										 }).bind('mouseout', function() { 
										 	
										 	 if ($(this).hasClass('zoom-icon-in'))
										 		$(this).removeClass('zoom-icon-in-hover');
										 	
										 	if ($(this).hasClass('zoom-icon-out'))
										 		$(this).removeClass('zoom-icon-out-hover');

										 }).bind('click', function() {

										 	if ($(this).hasClass('zoom-icon-in'))
										 		$('.magazine-viewport').zoom('zoomIn');
										 	else if ($(this).hasClass('zoom-icon-out'))	
												$('.magazine-viewport').zoom('zoomOut');

										 });

										 $('#canvas').hide();


										// Load the HTML4 version if there's not CSS transform

										yepnope({
											test : Modernizr.csstransforms,
											yep: ['lib/turn.js'],
											nope: ['lib/turn.html4.min.js'],
											both: ['lib/zoom.min.js', 'js/magazine.js', 'css/magazine.css'],
											complete: loadApp
										});

										</script>

										</body>
										</html>
								";

								fwrite($myfile, $txt);	
								fclose($myfile);

							  	shell_exec(" cd publications/$folder_name/ && zip -r $folder_name.zip *");

								$servername = "localhost";
								$username = "";
								$password = "";
								$dbname = "flipbook";

								// Create connection
								$conn = new mysqli($servername, $username, $password, $dbname);
								// Check connection
								if ($conn->connect_error) {
								    die("Connection failed: " . $conn->connect_error);
								}

								$sql = "INSERT INTO books (name, pages, create_date) VALUES ('".$folder_name."', '".$pageCount."', '".date("Y-m-d H:i:s")."');";
								
								if ($conn->query($sql) === TRUE) {
								    echo "New record created successfully";
								} else {
								    echo "Error: " . $sql . "<br>" . $conn->error;
								}
								$conn->close();
								?>
								<script type="text/javascript">
								<!--
								window.location = "index.php"
								//-->
								</script>
								<?php

							} else {
							   echo "Upload failed";
							}

						} else {
							?>
							<form enctype="multipart/form-data" action="" method="POST">
								<div class="form-group">
									<label>File input</label>
									<input type="file" name="userfile">
								</div>
								<button class="btn btn-default" type="submit">Submit PDF</button>
							</form>
							<?php
						}

						?>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->

</body>

</html>
